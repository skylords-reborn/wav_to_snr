use byteorder::{ReadBytesExt, BigEndian as BE};
use std::io::Read;
use crate::snr::{SNR_CHUNK_SIZE, SnrChunk, I_12_4};

pub const WAV_CHUNK_SIZE : usize = 128;

/// read `snr` chunk to per channel data, not directly to wav data!
fn read_snr_chunks(channels: usize, chunks: &[u8], channels_data: &mut [&mut[i16]]) {
    use std::slice::from_raw_parts;
    for c in 0..channels {
        let chunk = &chunks[c * SNR_CHUNK_SIZE..];
        let chunk_header = unsafe{from_raw_parts(chunk.as_ptr() as *const i16, 8)};
        let mut snr_chunk = SnrChunk::default();
        for n in 0..4 {
            snr_chunk.header[n].offset = I_12_4::from_i16(chunk_header[n * 2 + 0]);
            snr_chunk.header[n].shift = I_12_4::from_i16(chunk_header[n * 2 + 1]);
        }
        let chunk_data = &chunk[16..];
        for m in 0..15 {
            for n in 0..4 {
                let byte = chunk_data[m * 4 + n];
                snr_chunk.block[n][m] = byte;
            }
        }

        for n in 0..4 {
            snr_chunk.to_channel_data(n, &mut channels_data[c][n * 32..]);
        }

    }
}

pub(crate) fn read_snr_chunks_to_wav_chunk(channels: usize, chunks: &[u8], wav_data: &mut[i16]) {
    let c0 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c1 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c2 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c3 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c4 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c5 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let c6 = &mut[i16::MIN;WAV_CHUNK_SIZE];
    let channels_data : &mut [&mut [i16]] = &mut [c0, c1, c2, c3, c4, c5, c6];
    read_snr_chunks(channels, chunks, channels_data);
    for s in 0..WAV_CHUNK_SIZE {
        for c in 0..channels {
            wav_data[c + s * channels] = channels_data[c][s];
        }
    }
}

/// for sanity check, not meant to be real use case
pub fn snr_to_wav(string_path: &str) -> Result<(), std::io::Error> {
    let path = std::path::Path::new(string_path);
    let mut file = std::fs::File::open(path)?;
    let codec = file.read_u8()?;
    if codec != 4 {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            format!("Codec {} is not supported", codec)));
    }

    let channels = file.read_u8()? as usize;
    let channels = (channels / 4) + 1;
    let sample_rate = file.read_u16::<BE>()?;
    let total_samples = file.read_u32::<BE>()? as usize;
    let total_file_size = file.read_u32::<BE>()?; // without previous 8 bytes
    if file.read_u32::<BE>()? as usize != total_samples {
        return Err(std::io::Error::new(
            std::io::ErrorKind::InvalidData,
            "total sample count is expected to be same"));
    }

    let mut snr_data = Vec::with_capacity(total_file_size as usize - 8);
    file.read_to_end(&mut snr_data)?;
    let header = wav::header::Header::new(
        wav::WAV_FORMAT_PCM, channels as u16,
        sample_rate as u32, 16);
    let total_samples = ((total_samples + (WAV_CHUNK_SIZE-1)) / WAV_CHUNK_SIZE) * WAV_CHUNK_SIZE;
    let mut data = vec![0;total_samples * channels];

    let chunks_size = SNR_CHUNK_SIZE * channels;
    let wav_chunks_size = WAV_CHUNK_SIZE * channels;
    for i in 0..total_samples / WAV_CHUNK_SIZE {
        let chunks = &snr_data[i * chunks_size..(i+1) * chunks_size];
        read_snr_chunks_to_wav_chunk(channels, chunks, &mut data[i * wav_chunks_size..]);
    }

    let mut wav_file = std::fs::File::create(path.with_extension("snr.wav"))?;
    wav::write(header, &wav::BitDepth::Sixteen(data), &mut wav_file)
}
