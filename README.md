# This project will probably be left abandoned in favor of superior:
https://github.com/jonwil/snrtool

# wav to snr
This repository aims to be a simple application that converts wav files to snr files used by BattleForge.

### How to build
`cargo build`, or `cargo build --release` should do the trick.

### How to run it
`cargo run "path_to_wav_file.wav"` two new files will be created `path_to_wav_file.snr` 
and `path_to_wav_file.snr.wav` to be able to directly play the file to hear how bad it sounds after compressing it.

There is an alternative way to use it with two parameters
`-d` and `"path_to_wav_file.snr"` for quick sanity checking, but for conversion this way you should use also ffmpeg, 
because it has more features, and gives more options.

### Limitations
- wav needs to be in `i16` format ([WAV.bt](https://www.sweetscape.com/010editor/repository/files/WAV.bt)) (To create wav in this format you can use: [FFmpeg](https://github.com/FFmpeg/FFmpeg) or if you do not like its console interface, you can use GUI applications like: [Shutter Encoder](https://www.shutterencoder.com/en/) or [Audacity](https://www.audacityteam.org/))

### Issues / TODO
- exact shift and offset index limits, and behavior
- better algorithm that will consider also surrounding samples for determining best configuration
- add more tests
- extract library, to not depend directly on console interface, and allow some integration to other apps
- remove limitation to `i16` wav
- allow more formats to be converted directly

### How to contribute
- create merge request with fix, or improvement
- or just report an issue

### What is snr
It is significantly compressed audio format with multiple versions, but the version is unfortunately not available in the file itself ☹️

Header contains:
- codec used (**1 B**)
- channel count (**1 B**) encoded as `(channel_count - 1) * 4` so 0 = mono, 4 = stereo, 12 = 4 channels, 20 = 5.1
- sampling rate (**2 B** BigEndian)
- samples count per channel (**2 B** BigEndian)
- file size - 8 (**2 B** BigEndian), or file size starting from this field
- samples count per channel (**2 B** BigEndian) yes second time, and we have no clue why

Rest of file is in LittleEndian

Then blocks of samples (in case of multiple channels Block for channel 0 is followed by block for channel 1, after last channel is next block for channel 0, channel 1 and so on)

Each block is exactly **76 B**
- block start with **16 B** header that have 4 pairs of (lets call them A,B,C,D):
  - **4-bits** coefficient index
  - **12-bits** (rest of 16-bit number) is `i16` with bottom **4-bits** ignored, and it is first sample
  - **4-bits** shift value
  - **12-bits** (rest of 16-bit number) is `i16` with bottom **4-bits** ignored, and it is second sample
- for each pair in the header there are 15 bytes of data in pattern ABCDABCDABCD...
- each of the bytes is split to two **4-bit** nibbles, and the top one is used first
- you can look this up in the code how the **4-bit** value, coefficient index, shift value, and previous two samples are used to calculate next sample (which will be previous sample for the next **4-bit** value)
- each of the sections A, B, C, D is decompressed to 32 `i16` values that are stored in (mono) wav file AAA...ABBB...BCCC...CDDD...D and then again As from next block

### Resources
- [FFmpeg](https://github.com/FFmpeg/FFmpeg) big source of inspiration for snr to wav conversion.
- [EA Command And Conquer 3 Audio Codec](https://wiki.multimedia.cx/index.php?title=EA_Command_And_Conquer_3_Audio_Codec) description of EA's format
- [EA SAGE Audio Files](https://wiki.multimedia.cx/index.php/EA_SAGE_Audio_Files) description of EA's format
- [Sims 3:0x01A527DB](http://simswiki.info/wiki.php?title=Sims_3:0x01A527DB) description of EA's format
- [Audacity](https://www.audacityteam.org/) visualization of audio (uses FFmpeg)
- [Shutter Encoder](https://www.shutterencoder.com/en/) GUI converter for audio (uses FFmpeg)
- [010 Editor](https://www.sweetscape.com/010editor/) binary "visualization" of the audio files
- [WAV.bt](https://www.sweetscape.com/010editor/repository/files/WAV.bt) wav specification for someone who know nothing about audio, but only computers and numbers

### Disclosure
EA has not endorsed and does not support this product.
